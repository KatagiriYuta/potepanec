require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "#show" do
    let(:product) { create(:product) }

    before do
      get potepan_product_path(product.id)
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "returns a 200 response" do
      expect(response).to have_http_status 200
    end

    it "show product name" do
      expect(response.body).to include(product.name)
    end

    it "show product price" do
      expect(response.body).to include(product.display_price.to_s)
    end

    it "show product description" do
      expect(response.body).to include(product.description)
    end

    it "assigns @product" do
      expect(assigns(:product)).to eq product
    end
  end
end
